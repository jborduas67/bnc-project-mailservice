FROM node:10.12 as builder
WORKDIR /tmp
COPY package*.json ./
RUN npm install

COPY . .
EXPOSE 8080

CMD [ "npm", "start" ]